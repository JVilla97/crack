#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    int count = 0;
    char *hash;
    char line[50];

    if (argc < 3)
    {
        printf("You need to supply two filenames\n");
        exit(1);
    }
    if (strcmp(argv[1], argv[2]) == 0)
    {
        printf("Output file is same as input file\n");
        exit(1);
    }
    FILE *p = fopen(argv[1], "r");
    if (!p)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    FILE *h = fopen(argv[2], "w");
    if (!h)
    {
        printf("Can't open %s for writing\n", argv[2]);
        exit(1);
    }
    while(fgets(line, 50, p) != NULL)
    {
        //Gets the length of each line.
        int length = strlen(line);
        //Check for spaces at the end of each word, if there is a space substract 1 character, if not don't substract a character.
        if (line[strlen(line)-1] == '\n' || line[strlen(line)-1] == '\r')
        {
            length--;
        }
        //Call 'md5.c'.  
        hash = md5(line, length);
        // Write line to second file
        fprintf(h, "%s\n", hash);
        //Free willy...I mean hash.
        free(hash);
    }
    //Close the files.
    fclose(p);
    fclose(h);
}